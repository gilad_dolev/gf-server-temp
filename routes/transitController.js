const express = require('express');
const router = express.Router();

const dbs = require("../models");
const Transit = dbs.transit;
const Op = dbs.Sequelize.Op;

router.get('/', getTransit);
router.post('/add', addStation);
router.post('/add-bulk', addBulkTransit);

// GET /transit
function getTransit(req, res) {
    Transit.findAll()
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while getting transit."
            });
        });
}

// POST /transit/add
function addStation(req, res) {
    Transit.create(req.body)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating transit."
            });
        });
}

// POST /transit/add-bulk
function addBulkTransit(req, res) {
    Transit.bulkCreate(req.body)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while bulk creating transit."
            });
        });
}

module.exports = router;