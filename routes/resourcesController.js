let attributes = require('../attributes.js');
const cron = require('node-cron');
const express = require('express');
const router = express.Router();

const dbs = require("../models");
const Resources = dbs.resources;
const Op = dbs.Sequelize.Op;
const resourcesService = require('../services/resourcesService')(dbs);


router.get('/locations', getResourcesLocation);
router.get('/short', getResourcesByColumns);
router.post('/add', addResource);
router.post('/add-bulk', addBulkResources);
router.get('/', getResources);

console.log(attributes)

//  attributes=['id', 'resource_type','line_date']
// GET /resources/short
function getResourcesByColumns(req, res) {
        Resources.findAll({ 
            attributes })
            .then(data => {
                res.send(data);
            })
            .catch(err => {
                res.status(500).send({
                    message:
                        err.message || "Some error occurred while getting resources."
                });
            });
    }
// GET /resources
function getResources(req, res) {
    let resourcesData;


    if (req.query.lastId || req.query.lastDate || req.query.company_id || req.query.zone || req.query.lastLocationDate) {
        // TODO only some attributes
        resourcesData = resourcesService.getResourcesByIdOrDateUpdated(parseInt(req.query.lastId), req.query.lastDate, req.query.company_id, req.query.zone, req.query.lastLocationDate)
    } else {
        resourcesData = Resources.findAll();
    }
        //TODO - this must use company_id and Zones !!!!!!!!  
    resourcesData.then(data => {
        res.send(data);
    })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while getting resources."
            });
        });
}
//todo send relevant columns only
function getResourcesLocation(req, res) {
    let resourcesData;
    console.log("at getResourcesLocation ----")
    // if (req.query.lastId || req.query.company_id || req.query.zone ) {
    if (req.query.lastId || req.query.company_id || req.query.zone || req.query.lastLocationDate) {
        // resourcesData = resourcesService.getResourcesByIdOrDateUpdated(parseInt(req.query.lastId),  req.query.company_id, req.query.zone)
        resourcesData = resourcesService.getResourcesByIdOrDateUpdated(parseInt(req.query.lastId),  req.query.company_id, req.query.zone, req.query.lastLocationDate)
    } else {
        resourcesData = Resources.findAll();
    }
        //TODO - this must use company_id and Zones !!!!!!
    resourcesData.then(data => {
        res.send(data);
    })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while getting resources."
            });
        });
}
// POST /resources/add
function addResource(req, res) {
    Resources.create(req.body)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating resources."
            });
        });
}

// POST /resources/add-bulk
function addBulkResources(req, res) {
    Resources.bulkCreate(req.body)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while bulk creating resources."
            });
        });
}

module.exports = router;