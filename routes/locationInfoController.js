let attributes = require('../attributes.js');
const cron = require('node-cron');
const express = require('express');
const router = express.Router();

const dbs = require("../models");
const LocationInfo = dbs.location_info;
const Op = dbs.Sequelize.Op;

router.get('/', getLocationInfo);

// GET /location-info
function getLocationInfo(req, res) {
    let resourcesData = LocationInfo.findAll();

    resourcesData.then(data => {
        res.send(data);
    })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while getting location info."
            });
        });
}
module.exports = router;