const express = require('express');
const router = express.Router();
// var query = require('../db/db').query;

const dbs = require("../models");
const Vehicles = dbs.vehicles;
const Op = dbs.Sequelize.Op;

router.get('/', getVehicles);
router.post('/add', addVehicle);
router.post('/add-bulk', addBulkVehicles);

// GET /vehicles
function getVehicles(req, res) {
    Vehicles.findAll()
        .then(data => {
            res.status(200).send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while getting vehicles."
            });
        });
}

// function getVehicles(req, res) {
//     try {
//         query("SELECT * FROM vehicles;", function (result, fields) {
//             res.send({ success: true, result: result });
//         }, function (err) {
//             res.send({ success: false, message: 'error at getVehicles' })
//         });
//     }
//     catch {
//         console.log("ERR at getVehicles");
//         return res.status(401).json({ message: "ERR at getVehicles" })
//     }
// }

// POST /vehicles/add
function addVehicle(req, res) {
    Vehicles.create(req.body)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating vehicles."
            });
        });
}

// POST /vehicles/add-bulk
function addBulkVehicles(req, res) {
    Vehicles.bulkCreate(req.body)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while bulk creating vehicles."
            });
        });
}

module.exports = router;