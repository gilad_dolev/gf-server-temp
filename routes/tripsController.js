const express = require('express');
const router = express.Router();

const dbs = require("../models");
const Trips = dbs.trips;
const Op = dbs.Sequelize.Op;

router.get('/', getTrips);
router.get('/toresource', getTripsById);
router.post('/add', addStation);
router.post('/add-bulk', addBulkTrips);

// GET /trips
function getTrips(req, res) {
    Trips.findAll()
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while getting trips."
            });
        });
}
// GET trips/toresource?resource_id=2&company_id=1
function getTripsById(req, res) {

    Trips.findAll({
        where: {
          resource_id:req.query.resource_id,
          company_id:req.query.company_id

        }
     })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while getting trips."
            });
        });
}

    // POST /trips/add
    function addStation(req, res) {
        Trips.create(req.body)
            .then(data => {
                res.send(data);
            })
            .catch(err => {
                res.status(500).send({
                    message:
                        err.message || "Some error occurred while creating trips."
                });
            });
    }

// POST /trips/add-bulk
function addBulkTrips(req, res) {
    Trips.bulkCreate(req.body)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while bulk creating trips."
            });
        });
}

module.exports = router;