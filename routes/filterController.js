const express = require("express");
const router = express.Router();
const config = require("../config.json");
const dbs = require("../models");
const Resources = dbs.resources;
const Op = dbs.Sequelize.Op;
const query = require("../db/db").query;
router.get("/", getFilteredData);
router.get("/chosen", getSelectedItems);

// will move methods in service during migration to AWS

// GET Typed in filter on each key stroke to fill autocomplete options
function getFilteredData(req, res) {
  let filterData = req.query.data;
  let car = "car";
  let driver = "driver";
  let data = [];
  let zone = "";

  Resources.findAll({
    attributes: [
      "resource_type",
      "line_description",
      "car_number",
      "driver_nick_name",
      "driver_first_name",
      "driver_last_name",
      "id",
      "company_id",
      "zone",
    ],
    limit: 5,
    where: {
      zone: {
        [Op.like]: `%${zone}%`,
      },

      [Op.and]: [
        {
          car_number: {
            [Op.like]: `%${filterData}%`,
          },
        },
        {
          resource_type: {
            [Op.like]: `%${car}%`,
          },
        },
      ],
    },
  }).then((foundCars) => {
    let _cars = foundCars;
    _cars.map((car) => (car.resource_type = "car"));
    data.push(..._cars);
  });

  Resources.findAll({
    attributes: [
      "car_number",
      "resource_type",
      "line_description",
      "current_trip_name",
      "driver_nick_name",
      "driver_first_name",
      "driver_last_name",
      "id",
      "company_id",
      "zone",
    ],
    limit: 5,
    where: {
      zone: {
        [Op.like]: `%${zone}%`,
      },
      resource_type: {
        [Op.like]: `%${driver}%`,
      },
     
      [Op.or]: [
        {
          driver_first_name: {
            [Op.like]: `%${filterData}%`,
          },
        },
        {
          driver_last_name: {
            [Op.like]: `%${filterData}%`,
          },
        },
        {
          driver_nick_name: {
            [Op.like]: `%${filterData}%`,
          },
        },
      ],
    },
  })
    .then((foundDrivers) => {
      let _drivers = foundDrivers;
      _drivers.map((driver) => (driver.resource_type = "driver"));
      data.push(..._drivers);
      res.send(data);
    })

    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while getting resources.",
      });
    });
}

// function getFilteredData(req, res) {
//   let filterData = req.query.data;
//   let zone = "1,2";
//   Resources.findAll({
//     attributes: [
//       "resourceType",
//       "line_description",
//       "car_number",
//       "driverNickname",
//       "driverFirstName",
//       "driverLastName",
//       "id",
//       "company_id",
//       "zone",
//     ],
//     limit: 5,
//     where: {
//       zone: {
//         [Op.like]: `%${zone}%`,
//       },

//       [Op.or]: [
//         {
//           resourceType: {
//             [Op.like]: `%${filterData}%`,
//           },
//         },
//         {
//           line_description: {
//             [Op.like]: `%${filterData}%`,
//           },
//         },
//         {
//           car_number: {
//             [Op.like]: `%${filterData}%`,
//           },
//         },
//         {
//           driverFirstName: {
//             [Op.like]: `%${filterData}%`,
//           },
//         },
//         {
//           driverLastName: {
//             [Op.like]: `%${filterData}%`,
//           },
//         },
//         {
//           driverNickname: {
//             [Op.like]: `%${filterData}%`,
//           },
//         },
//       ],
//     },
//   })
//     .then((data) => {
//       res.send(data);
//     })
//     .catch((err) => {
//       res.status(500).send({
//         message: err.message || "Some error occurred while getting resources.",
//       });
//     });
// }

async function getSelectedItems(req, res) {
  let filterData = req.query;
  let rules = [];

  if (filterData.drivers !== "") {
    let driversWhere = [];
    for (
      let i = 0;
      i < (filterData.drivers.match(/,/g) || []).length + 1;
      i++
    ) {
      driversWhere.push({
        driver_nick_name: {
          [Op.like]: `%${filterData.drivers.split(",")[i]}%`,
        },
      });
      driversWhere.push({
        driver_first_name: {
          [Op.like]: `%${filterData.drivers.split(",")[i]}%`,
        },
      });
      driversWhere.push({
        driver_last_name: {
          [Op.like]: `%${filterData.drivers.split(",")[i]}%`,
        },
      });
    }
    rules.push({
      [Op.or]: driversWhere,
    });
  }
  // loop in cars
  if (filterData.cars !== "") {
    let carsWhere = [];
    for (let i = 0; i < (filterData.text.match(/,/g) || []).length + 1; i++) {
      carsWhere.push({
        car_number: {
          [Op.like]: `%${filterData.cars.split(",")[i]}%`,
        },
      });
    }
    rules.push({
      [Op.or]: carsWhere,
    });
  }
  // loop in text
  if (filterData.text !== "") {
    let textWhere = [];
    for (let i = 0; i < (filterData.text.match(/,/g) || []).length + 1; i++) {
      textWhere.push({
        line_description: {
          [Op.like]: `%${filterData.text.split(",")[i]}%`,
        },
      });
      textWhere.push({
        resource_type: {
          [Op.like]: `%${filterData.text.split(",")[i]}%`,
        },
      });
      textWhere.push({
        driver_first_name: {
          [Op.like]: `%${filterData.text.split(",")[i]}%`,
        },
      });
      textWhere.push({
        driver_last_name: {
          [Op.like]: `%${filterData.text.split(",")[i]}%`,
        },
      });
      textWhere.push({
        driver_nick_name: {
          [Op.like]: `%${filterData.text.split(",")[i]}%`,
        },
      });
      textWhere.push({
        car_number: {
          [Op.like]: `%${filterData.text.split(",")[i]}%`,
        },
      });
    }
    rules.push({
      [Op.or]: textWhere,
    });
  }

  let r = await Resources.findAll({
    attributes: [
      "resource_type",
      "line_description",
      "current_trip_name",
      "car_number",
      "driver_nick_name",
      "driver_first_name",
      "driver_last_name",
      "id",
      "company_id",
      "zone",
    ],
    where: rules,
  })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while getting selected resources.",
      });
    });
}

module.exports = router;
