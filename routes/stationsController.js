const express = require('express');
const router = express.Router();

const dbs = require("../models");
const Stations = dbs.stations;
const Op = dbs.Sequelize.Op;

router.get('/', getStations);
router.post('/add', addStation);
router.post('/add-bulk', addBulkStations);

// GET /stations
function getStations(req, res) {
    Stations.findAll()
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while getting stations."
            });
        });
}

// POST /stations/add
function addStation(req, res) {
    Stations.create(req.body)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating stations."
            });
        });
}

// POST /stations/add-bulk
function addBulkStations(req, res) {
    Stations.bulkCreate(req.body)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while bulk creating stations."
            });
        });
}

module.exports = router;