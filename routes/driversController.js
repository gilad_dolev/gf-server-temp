const express = require('express');
const router = express.Router();

const dbs = require("../models");
const Drivers = dbs.drivers;
const Op = dbs.Sequelize.Op;

router.get('/', getDrivers);
router.post('/add', addDriver);
router.post('/add-bulk', addBulkDrivers);

// GET /drivers
function getDrivers(req, res) {
    let where = {};

    if (req.query.id) {
        where.id = parseInt(req.query.id);
    }

    Drivers.findAll(where)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while getting drivers."
            });
        });
}

// POST /drivers/add
function addDriver(req, res) {
    Drivers.create(req.body)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating drivers."
            });
        });
}

// POST /drivers/add-bulk
function addBulkDrivers(req, res) {
    Drivers.bulkCreate(req.body)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while bulk creating drivers."
            });
        });
}

module.exports = router;