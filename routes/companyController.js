const express = require('express');
const router = express.Router();

const dbs = require("../models");
const Company = dbs.company;
const Op = dbs.Sequelize.Op;

router.get('/', getCompany);
router.post('/add', addStation);
router.post('/add-bulk', addBulkCompany);

// GET /company
function getCompany(req, res) {
    Company.findAll()
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while getting company."
            });
        });
}

// POST /company/add
function addStation(req, res) {
    Company.create(req.body)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating company."
            });
        });
}

// POST /company/add-bulk
function addBulkCompany(req, res) {
    Company.bulkCreate(req.body)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while bulk creating company."
            });
        });
}

module.exports = router;