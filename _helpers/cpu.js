




const os = require('os');
console.log("CPU's data :");
console.log("TotalMemory on server :" + Math.floor(os.totalmem() / 1000000) + "MegaByte   | free memory on server:" + Math.floor(os.freemem() / 1000000) + "MegaByte");
const _ = require("underscore");
const query = require('./db/db').query;


const interval = parseInt(config.cpuInterval);
const old = _.map(os.cpus(), function (cpu) { return cpu.times; })
app.use(express.json({ limit: '10kb' })); // Body limit is 10 
//npm install express-rate-limit
const limit = rateLimit({
  max: 500,// max requests
  windowMs: 60 * 60 * 1000, // 1 Hour
  message: 'Too many requests' // message to send
});
setInterval(function () {
    const result = [];
    const current = _.map(os.cpus(), function (cpu) { return cpu.times; })
    const totalCpu = 0;
  _.each(current, function (item, cpuKey) {
    result[cpuKey] = {}
    var oldVal = old[cpuKey];
    _.each(_.keys(item), function (timeKey) {
      if (timeKey == "idle") {
        var diff = (parseFloat((item[timeKey]) - parseFloat(oldVal[timeKey])) / parseFloat((interval * 100)));
        diff = 100 - diff;
        totalCpu += diff;
      }
    });
  });

  totalCpu /= current.length;


  query("insert into ultra.cpu_usage (cpu,created,memory,version) values (" + parseInt(totalCpu) + ",now(),'" + Math.floor(os.freemem() / 1000000) + "/" + Math.floor(os.totalmem() / 1000000) + "','"+version+"');")
  old = current;

}, (interval * 1000));