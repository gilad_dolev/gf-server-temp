const expressJwt = require('express-jwt');
const config = require('../config.json');
let db = require('../db/db');
let promiseQuery = require('../db/db').promiseQuery;
const utils = require('./utils');
const CryptoJS = require("crypto-js");
module.exports = { security };
let user;
function security() {
   console.log("at security checkUserLogIn", user);
    try {
        return [
            async (req, res, next) => {
                if (req) {
                    if (req && req.body) {
                        if(req.body.PUBLIC){
                            return next();
                        }
                        path = req.route.path ? utils.sanitize(req.route.path.replace("/", "")) : null;
                        console.log("at security checkUserLogIn path:", path);
                        if(req.body.user)
                            user = utils.sanitize(utils.decryptWithAES(req.body.user))
                        // var accessToken = req.body.accessToken;
                        // if(user.startsWith('public')){
                        //  return next();
                        // }

                        let cookie = req.headers.cookie;
                        // let token = req.headers.authorization.split(" ")[1];
                        let token =  req.body.accessToken;
                        let tokenPayLoad = utils.decode(token);
                        if(!tokenPayLoad || !tokenPayLoad[0] ){
                            console.log("Err No tokenPayLoad")
                            return false;
                        }
                        var tokenUserName = tokenPayLoad[0];
                        if (tokenUserName !== user) {
                            console.log("Err username and token user are not equal")
                            return false;
                        }

                    } else
                        return false;
                    // if (req.body) {
                    //     user = req.body.user ? utils.sanitize(req.body.user) : null;
                    //     appname = req.body.app ? utils.sanitize(req.body.app) : null;
                    // path = req.route.path ? req.route.path.replace("/", "") : null;
                    // }
                   
                    await checkUserLogIn(user, tokenUserName)
                        .then(result => {
                            // console.log("Inside checkSecurity , req.query.appname:", req.route.path, 'results:', result[0][0].success)
                            isSecurityPass = result[0][0].success;
                            if (isSecurityPass == "true")
                                return next();
                            return res.status(401).json({ message: 'Unauthorized role' });
                        }).catch(x => {
                            //console.log('pageim security: ' + x)
                            return res.status(401).json({ message: x })
                        });
                }
            }]
    }
    catch {
        logger('', 'ERR at checkSecurity')
        return res.status(401).json({ message: "Error at security" })
    }
}


async function checkUserLogIn(user) {
    try {
        console.log("at checkUserLogIn", user);

        const res = promiseQuery(`CALL ultra.security('${user}');`);
        return res;
    }
    catch {
        console.log("ERR at checkUserLogIn");
        return res.status(401).json({ message: "Error at checkUserLogIn" })
    }
}






function checkSecurityOld() {
    try {

        return [
            async (req, res, next) => {

                if (req) {

                    const user = utils.sanitize(decryptWithAES(req.body.user))
                    const accessToken = req.body.accessToken;

                    // let cookie = req.headers.cookie.split('token=')[1];
                    // let token = req.headers.authorization.split(" ")[1];
                    let token =  req.body.accessToken;
                    let tokenPayLoad = utils.decode(token);
                    let tokenUserName = tokenPayLoad[0];
                    let tokenTime = tokenPayLoad[1];
                    // let cookiePayLoad = utils.decode(cookie);
                    // let cookieUserName = cookiePayLoad[0];
                    // let cookieTime = cookiePayLoad[1];
                }
                if (req.body) {
                    user = req.body.user ? utils.sanitize(req.body.user) : null;
                    appname = req.body.app ? utils.sanitize(req.body.app) : null;
                    path = req.route.path ? req.route.path.replace("/", "") : null;

                    //   req.user.sub ? req.user.sub = utils.sanitize(req.user.sub) : '';
                    //   req.query.appname ? req.query.appname = utils.sanitize(req.query.appname) : '';
                    //   req.route.path ? req.route.path = utils.sanitize(req.route.path) : '';
                    //   req.user.sub ? req.user.sub = utils.sanitize(req.user.sub) : '';
                    //   req.query.search ? req.query.search = utils.sanitize(req.query.search) : '';
                    //   req.query.order_by ? req.query.order_by = utils.sanitize(req.query.order_by) : '';
                    //   req.query.itemsperpage ? req.query.itemsperpage = utils.sanitize(req.query.itemsperpage) : '';
                    //   req.query.checked ? req.query.checked = utils.sanitize(req.query.checked) : '';
                    //   req.query.name ? req.query.name = utils.sanitize(req.query.name) : '';
                    //   req.query.mobileNumber ? req.query.mobileNumber = utils.sanitize(req.query.mobileNumber) : '';
                    //   req.query.personalName ? req.query.personalName = utils.sanitize(req.query.personalName) : '';
                    //   req.query.value ? req.query.value = utils.sanitize(req.query.value) : '';
                    //   req.query.item ? req.query.item = utils.sanitize(req.query.item) : '';
                    //   req.query.itemsperpage ? req.query.itemsperpage = utils.sanitize(req.query.itemsperpage) : '';
                    //   req.query.currentpage ? req.query.currentpage = utils.sanitize(req.query.currentpage) : '';
                    //   req.query.key ? req.query.key = utils.sanitize(req.query.key) : '';
                    //   req.query.rowId ? req.query.rowId = utils.sanitize(req.query.rowId) : '';
                    //   req.connection.remoteAddress ? req.connection.remoteAddress = utils.sanitize(req.connection.remoteAddress) : '';
                    //   if (req.body && req.body.data) {
                    //       req.body.data.message ? req.body.data.message = utils.sanitize(req.body.data.message) : '';
                    //       req.body.data.name ? req.body.data.name = utils.sanitize(req.body.data.name) : '';
                    //       req.body.data.phone ? req.body.data.phone = utils.sanitize(req.body.data.phone) : '';
                    //       req.body.data.email ? req.body.data.email = utils.sanitize(req.body.data.email) : '';
                    //   }

                }
                //check for honny pot 
                if (req.body && req.body.data && req.body.data.name_ho && req.body.data.email_ho != '') {
                    console.log('fail-no permition  HONYPOT from client')
                    return res.status(401).json({ message: "fail-no permition" })
                }

                var isSecurityPass = 'false';
                //debugger;
                await checkSecurity(user, appname)
                    .then(result => {
                        isSecurityPass = result[0][0].success;
                        //console.log("isSecurityPass: ", isSecurityPass)
                        if (isSecurityPass == "true")
                            return next();
                        console.log("Unauthorized role 6 checkSecurity", user, appname);
                        return res.status(401).json({ message: 'Unauthorized ' });
                    }).catch(x => {
                        console.log('ultra security: ' + x)
                        return res.status(401).json({ message: x })
                    });
            }];
    }
    catch {
        console.log(`ERR at security  `)
        return res.status(401).json({ message: "Error at security" })
        //TODO  save with ip and time to database

    }
}