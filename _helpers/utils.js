

const expressJwt = require('express-jwt');
const config = require('../config.json');
const CryptoJS = require("crypto-js");
module.exports = { decode, sanitize, makeName, decryptWithAES };

const jwt = require('jsonwebtoken');

function decode(token) {
    console.log("at ..utils.js/decode");
    const payload = jwt.decode(token, config.secret, algorithms = "HS256");
    // console.log(payload);
    if (payload)
        return [payload.userName, payload.time, payload.user_id];
    else
        return null;

    var decoded = jwt.verify(token, config.secret);
    console.log(decoded.userName) // bar
}

function sanitize(string,light=false) {
    try {
        let keys=[];
        if(light){
             keys=[' or ',' or(',' ,concat ','concat','information_schema','updatexml','extractvalue','table_schema','\'',"'","*", "!","?","(",")","|",'name_const','cast',' and ',' and(']
        }
        else  keys=[' or ',' or(',' ,concat ','concat','information_schema','updatexml','extractvalue','table_schema','\'',"+","'","*", "$","!","?","(",")","|",'name_const',' cast ',' and ',' and(']
        let cleanString = '';
        if (string !== null && string !== ""){
            if (typeof string === 'string') {
                // cleanString = string.replace('\'', '').replace("'", '').replace("*", '').replace("$", '').replace("!", '').replace("?", '').replace("|", '');
                cleanString=string;
                keys.forEach(function(key){
                    cleanString = cleanString.replace(key, '')
                })
                return cleanString
            }
            else{
                return Number(string);
            }
           
        }
        else return '';
   
    }
    catch (e) {
        console.log(`ERR at sanitize  `)
        return json({ message: "Error at sanitize" })
    }
}
function makeName(length) {
    try {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        let charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
    catch {
        console.log(`ERR at makeName  `)
        return res.status(401).json({ message: "Error at sanitize" })
    }
}
function decryptWithAES(ciphertext) {
    try {
        const passphrase = config.loginEncript;
        const bytes = CryptoJS.AES.decrypt(ciphertext, passphrase);
        const originalText = bytes.toString(CryptoJS.enc.Utf8);
        return originalText;
    }
    catch {
        console.log(`ERR at decryptWithAES  `)
        return res.status(401).json({ message: "Error at sanitize" })
    }
};