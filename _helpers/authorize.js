const expressJwt = require('express-jwt');
const config = require('../config.json');
let db = require('../db/db');
const utils = require('./utils'); 
module.exports = { authorize, authorizeRegistered,isRegistered };



function isRegistered() {
    console.log('at isRegistered');
    return [
        
        // authenticate JWT token and attach user to request object (req.user)
        expressJwt({ secret: config.secret }),
        (req, res, next) => {
            const user=utils.sanitize(req.body.user) 
            // let cookie = req.headers.cookie.split('=')[1];
            let token =  req.body.accessToken;
            // let token = req.headers.authorization.split(" ")[1];
            let tokenPayLoad = utils.decode(token);
            let tokenUserName = tokenPayLoad[0];
            let tokenTime = tokenPayLoad[1];
            // let cookiePayLoad = utils.decode(cookie);
            // let cookieUserName = cookiePayLoad[0];
            // let cookieTime = cookiePayLoad[1];
            if (! tokenUserName || !tokenTime  || !user ) {
                try {
                    query("call ultra.is_log_in('" + tokenUserName + "','" + user + "');"
                        , (result, fields) => {
                            if (result[0][0].success == 'true') {
                                return next();
                                // return res.send({ success: 'true' });
                            }
                            setTimeout(function () {
                                return res.status(401).json({ success: false, message: 'Unauthorized role' });
                            }, 1000);
                        }, err => setTimeout(function () {
                            return res.status(401).json({ success: false, message: 'Unauthorized role' });
                        }, 1000));
                }
                catch (e) {
                    {
                        console.log(req.user.role, 'Unauthorized role1')
                        // user's role is not authorized
                        // return res.status(401).json({ message: 'Unauthorized role' });
                        setTimeout(function () {
                            return res.status(401).json({ success: false, message: 'Unauthorized role' });
                        }, 1000);

                    }
                }
            }
            else {
                console.log(req.user.role, 'Unauthorized role1')
                // user's role is not authorized
                setTimeout(function () {
                    return res.status(401).json({ success: false, message: 'Unauthorized role' });
                }, 1000);
            }




            if (roles.length && !roles.includes(req.user.role)) {
                console.log(req.user.role, 'Unauthorized role1')
                // user's role is not authorized
                return res.status(401).json({ message: 'Unauthorized role' });
            }




            if (roles.includes("admin")) {
                db.adminAuthenticate(req.user.sub, req.user.password, function () {
                    return next();
                }, function () {
                    console.log(req.user.sub, req.user.password, 'Unauthorized role2')
                    return res.status(401).json({ message: 'Unauthorized' })
                })
                return
            }

            db.freeUserAuthenticate(req.user.sub, function () {
                return next();
            }, function () {
                console.log(req.user.sub, 'Unauthorized role3')
                return res.status(401).json({ message: 'Unauthorized' })
            });
            return;


        }
    ];
}

function authorize(roles = []) {
    console.log(roles)
    console.log('at authorize-------------------', roles)
    // roles param can be a single role string (e.g. Role.User or 'User') 
    // or an array of roles (e.g. [Role.Admin, Role.User] or ['Admin', 'User'])
    if (typeof roles === 'string') {
        roles = [roles];
    }
    return [
        // authenticate JWT token and attach user to request object (req.user)
        expressJwt({ secret: config.secret }),
        // authorize based on user role
        (req, res, next) => {

            if (roles.length && !roles.includes(req.user.role)) {
                console.log(req.user.role, 'Unauthorized role1')
                // user's role is not authorized
                return res.status(401).json({ message: 'Unauthorized role' });
            }

            if (roles.includes("admin")) {
                db.adminAuthenticate(req.user.sub, req.user.password, function () {
                    return next();
                }, function () {
                    console.log(req.user.sub, req.user.password, 'Unauthorized role2')
                    return res.status(401).json({ message: 'Unauthorized' })
                })
                return
            }

            db.freeUserAuthenticate(req.user.sub, function () {
                return next();
            }, function () {
                console.log(req.user.sub, 'Unauthorized role3')
                return res.status(401).json({ message: 'Unauthorized' })
            });
            return;


        }
    ];
}



function authorizeRegistered(roles = []) {

    if (typeof roles === 'string') {
        roles = [roles];
    }
    return [
        expressJwt({ secret: config.secretRegistered }),
        (req, res, next) => {
            if (roles.length && !roles.includes(req.user.role))
                return res.status(401).json({ message: 'Unauthorized role5' });
            return next();
        }
    ];
}