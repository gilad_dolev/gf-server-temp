const { Op } = require("sequelize");
const moment = require('moment');

module.exports = (function (db) {
    return {
        addResource,
        updateResource,
        incrementResource,
        getAllResources,
        getResource,
        getResourcesByIdOrDateUpdated
    };

    function addResource(data) {
        return db.resources.create(data)
    }

    function updateResource(data, where) {
        return db.resources.update(data, { where: where })
    }

    function incrementResource(column, options) {
        return db.resources.increment(column, options);
    }

    function getAllResources(where) {
        return db.resources.findAll({ where: where });
    }

    function getResource(where) {
        return db.resources.findOne({ where: where });
    }

    function getResourcesByIdOrDateUpdated(lastId = null, company_id = null, zone = null, lastLocationDate = null) {
        let where = [];
        // lastDate = lastDate ? moment(lastDate).format('YYYY-MM-DD HH:mm:ss') : lastDate; //actually not needed
        lastLocationDate = lastLocationDate ? moment(lastLocationDate).format('YYYY-MM-DD HH:mm:ss') : null; //actually not needed

        if (lastId) {
            where.push({
                id: {
                    [Op.gt]: lastId
                }
            })
        }

        // if (lastDate) {
        //     where.push({
        //         updatedAt: {
        //             [Op.gt]: lastDate
        //         }
        //     })
        // }

        if (lastLocationDate) {
            where.push({
                position_last_update: {
                    [Op.gt]: lastLocationDate
                }
            })
        }

        // if (lastId && lastDate) {
        //     where.push({
        //         [Op.or]: [
        //             {
        //                 id: {
        //                     [Op.gt]: lastId
        //                 }
        //             },
        //             {
        //                 updatedAt: {
        //                     [Op.gt]: lastDate
        //                 }
        //             }
        //         ]
        //     });
        // }

        if (company_id) {
            where.push({ company_id: company_id })
        }

        if (zone) {
            where.push({ zone: zone })
        }

        return db.resources.findAll({ where: where }); // Do not change it to (where)
    }

})
