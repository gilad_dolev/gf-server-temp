module.exports = (sequelize, DataTypes) => {
    const Vehicles = sequelize.define("vehicles", {
        car_id: {
            type: DataTypes.INTEGER,
        },
        car_code: {
            type: DataTypes.STRING,
        },
        car_number: {
            type: DataTypes.STRING,
        },
        type: {
            type: DataTypes.STRING
        },
        v_licence: {
            type: DataTypes.STRING
        },
        driver_id: {
            type: DataTypes.INTEGER
        },
        driver_fname: {
            type: DataTypes.STRING
        },
        driver_lname: {
            type: DataTypes.STRING
        },
        driver_mobile1: {
            type: DataTypes.STRING
        },
        v_size: {
            type: DataTypes.INTEGER
        },
        v_seats: {
            type: DataTypes.INTEGER
        },
        v_latitude: {
            type: DataTypes.STRING(15)
        },
        v_longitude: {
            type: DataTypes.STRING(15)
        },
        brand: {
            type: DataTypes.STRING
        },
        model: {
            type: DataTypes.STRING
        },
        icon_size: {
            type: DataTypes.STRING
        },
        scope: {
            type: DataTypes.STRING
        },
        is_active: {
            type: DataTypes.INTEGER
        }
    });
    return Vehicles;
};
