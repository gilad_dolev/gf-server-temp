module.exports = (sequelize, DataTypes) => {
    const Drivers = sequelize.define("drivers", {
        company_id: {
            type: DataTypes.INTEGER
        },
        account_code: {
            type: DataTypes.STRING,
        },
        acc_name: {
            type: DataTypes.STRING,
        },
        given_name: {
            type: DataTypes.STRING,
        },
        nick_name: {
            type: DataTypes.STRING
        },
        first_name: {
            type: DataTypes.STRING,
        },
        last_name: {
            type: DataTypes.STRING
        },
        display_name: {
            type: DataTypes.STRING
        },
        phone_number: {
            type: DataTypes.STRING,
        },
        is_active: {
            type: DataTypes.INTEGER
        }
    });
    return Drivers;
};