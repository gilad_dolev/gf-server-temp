module.exports = (sequelize, DataTypes) => {
    const Stations = sequelize.define("stations", {
        code: {
            type: DataTypes.INTEGER,
        },
        start_time: {
            type: DataTypes.DATE,
        },
        station_status: {
            type: DataTypes.INTEGER,
        },
        station_city: {
            type: DataTypes.STRING
        },
        station_street: {
            type: DataTypes.STRING
        },
        station_house: {
            type: DataTypes.STRING
        },
        station_index: {
            type: DataTypes.INTEGER
        },
        last_action: {
            type: DataTypes.STRING
        },
        actual_start_time: {
            type: DataTypes.DATE
        },
        latitude: {
            type: DataTypes.STRING
        },
        longitude: {
            type: DataTypes.STRING
        },
        place: {
            type: DataTypes.STRING
        },
        is_active: {
            type: DataTypes.INTEGER
        }
    });
    return Stations;
};