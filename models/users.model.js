module.exports = (sequelize, DataTypes) => {
    const Users = sequelize.define("users", {
        company_id: {
            type: DataTypes.INTEGER
        },
        phone_numer: {
            type: DataTypes.STRING,
        },
        user_name: {
            type: DataTypes.STRING
        },
        scopes: {
            type: DataTypes.STRING
        },
        valid_up_to: {
            type: DataTypes.DATE
        },
        role: {
            type: DataTypes.INTEGER,
        }
    });
    return Users;
};