const config = require('./config.json');
const application_root = __dirname;
const express = require('express');
// const vhost = require('vhost');
const path = require('path');
var mysql = require('mysql');
var fs = require('fs');
const jwt = require('jsonwebtoken');
const expressJwt = require('express-jwt');
const morgan = require('morgan');
const authorize = require('./_helpers/authorize');
const security = require('./_helpers/security');
const accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' })
const query = require('./db/db').query;
const adminAuthenticate = require('./db/db').adminAuthenticate;
// const rateLimit = require("express-rate-limit");
const helmet = require('helmet');
const xss = require('xss-clean');
const version = config.version;
const db = require("./models");
const cors = require('cors');

const app = express();
app.use(cors({ origin: "http://localhost:3000", credentials: true }));
// Increase the post limit
// app.use(cors());
// Increase the post limit
app.use(express.json({ limit: '50mb' }));
// app.use(express.urlencoded({ limit: '50mb' }));

// app.use(function (req, res, next) {
//     // Website you wish to allow to connect
//     res.setHeader('Access-Control-Allow-Origin', '*');
//     // Request methods you wish to allow
//     res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
//     // Request headers you wish to allow
//     res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
//     // Set to true if you need the website to include cookies in the requests sent
//     // to the API (e.g. in case you use sessions)
//     res.setHeader('Access-Control-Allow-Credentials', true);
//     // Pass to next layer of middleware
//     next();
// });


const port = 4000;
app.use(express.json());
app.use(require('./routes'));

app.use(morgan('combined', { stream: accessLogStream }))


const logRequestStart = (req, res, next) => {
    query(`call ultra.log('${req.connection.remoteAddress} ${req.method} ${req.originalUrl}')`, '');
    next()
}

app.get('/db', (req, res) => {
    db.sequelize.sync({ alter: true }).then(function () {
        res.json("Successfully altered DB")
    });
});

app.listen(port, () => {
    // connect();
    console.log(`MAIN Server listening at http://localhost:${port}`)
    console.log()
    console.log(`DB INFO: After editing dbconfig.json with your local db config, go to http://localhost:${port}/db to create or update tables.`)
    // console.log(`After that open http://localhost:${port}/create-tables to create the tables`)
    // console.log(`OR open http://localhost:${port}/update-tables to update columns (if you have changed the models)`)
    // console.log(`OR open http://localhost:${port}/force-update - IT WILL DELETE ALL YOUR DB DATA TOO, but the position for each column will be as the models`)
    console.log()
})
