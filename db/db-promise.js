let promise = require('mysql-promise');
var config = require('../../../../Users/Jonathan Daniel/Desktop/temp server/config.json');

module.exports = {public,private};

function public(query)
{
    var conection = promise();
    conection.configure(config.connection);
    return conection.query(query);
}

function private(user,password,query)
{
    var conection = promise();
    conection.configure({
        host: "localhost",
        user: user,
        password: password,
        database: 'mysql',
        port: 3306
      });
    return conection.query(query);
}
