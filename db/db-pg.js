const promise =require('bluebird');
//import {IInitOptions, IDatabase, IMain, Column} from 'pg-promise';
const dbConfig = require('../../../../Users/Jonathan Daniel/Desktop/temp server/config.json');


const initOptions = {
    // global event notification;
    error(error, e) {
        if (e.cn) {
            // A connection-related error;
            //
            // Connections are reported back with the password hashed,
            // for safe errors logging, without exposing passwords.
            console.log('CN:', e.cn);
            console.log('EVENT:', error.message || error);
        }
    }
  };

  
const pgPromise = require('pg-promise')(initOptions);

// using an invalid connection string:
const dbpg = pgPromise('postgresql://'+dbConfig.pgConn.user+':'+dbConfig.pgConn.password+'@'+dbConfig.pgConn.host+':5433/'+dbConfig.pgConn.database);

dbpg.connect()
  .then(obj => {
      // Can check the server version here (pg-promise v10.1.0+):
      const serverVersion = obj.client.serverVersion;

      obj.done(); // success, release the connection;

      console.log('postgresql connection success')
   
  })
  .catch(error => {
      console.log('Postgresql ERROR:', error.message || error);
  });

  


module.exports = {db:dbpg};
