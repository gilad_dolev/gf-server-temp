var config = require('../config.json');

var mysql = require('mysql');

function db() {
   return mysql.createConnection(config.connection);
  //return  gpdb.db;
}

function query(query, callback, onerror) {
  var con = db();
  con.connect(function (err) {
    if (err) {
      console.log('err query connected',err);
      con.end();
      if (onerror)
        return onerror(err.message)
      return console.log('err1', err.message)
    }
    con.query(query,
      function (err, result, fields) {
        con.end();
        if (err && onerror) {
          if (onerror)
            return onerror(err.message)
          return console.log('err2', err.message)
        }
        if (callback) callback(result, fields);
      });
  });

}


function promiseQuery(query) {
  //console.log('at promiseQuery') 
  return new Promise((resolve, reject) => {
    var con = db();
    con.connect(function (err) {
      if (err) {
        console.log(" connection lost :" + err)
        con.end();
        return reject(err.message)
      }
      con.query(query,
        function (err, result, fields) {
          con.end();
          if (err) {

            console.log(" execution error :" + err)

            return reject(err.message)
          }
          resolve(result, fields);
        });
    });
  });

}

function adminAuthenticate(user, password, callback, onerror) {
  var connData = config.connection;
  connData.user = user;
  connData.password = password;
  var con = mysql.createConnection(connData);

  con.connect(function (err) {
    if (err) { con.end(); return onerror(err.message) }
    con.query("show databases;",
      function (err, result, fields) {
        if (err && onerror) return onerror(err.message);
        con.end();

        if (result.length > 0)
          return callback();

        return onerror("not admin");
      });
  });

}



function freeUserAuthenticate(user, callback, onerror) {

  var connData = config.connection;

  var con = mysql.createConnection(connData);

  con.connect(function (err) {
    if (err) { return onerror(err.message) }
    con.query("SELECT * FROM infra.devices where name = '" + user + "';",
      function (err, result, fields) {
        if (err && onerror) return onerror(err.message);
        con.end();

        if (result.length > 0)
          return callback();

        return onerror("not exists user");
      });
  });

}

module.exports = { query, adminAuthenticate, freeUserAuthenticate, promiseQuery };