CREATE TABLE `vehicles` (
  `v_Id` int(11) NOT NULL AUTO_INCREMENT,
  `v_licence` varchar(45) DEFAULT NULL,
  `driver_id` varchar(45) DEFAULT NULL,
  `driver_fname` varchar(45) DEFAULT NULL,
  `driver_lname` varchar(45) DEFAULT NULL,
  `driver_mobile1` varchar(45) DEFAULT NULL,
  `v_size` varchar(45) DEFAULT NULL,
  `v_seats` varchar(45) DEFAULT NULL,
  `v_latitude` decimal(10,0) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL,
  `last_update` datetime DEFAULT CURRENT_TIMESTAMP,
  `v_longitude` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`v_Id`),
  UNIQUE KEY `v_licence_UNIQUE` (`v_licence`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;


UPDATE gf-temp.user SET Password=PASSWORD('1234123412341234') WHERE User=('root'); 