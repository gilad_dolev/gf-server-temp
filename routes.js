const express = require('express');
const couponRoutes = require('./routes/couponRoutes');
const vehiclesController = require('./routes/vehiclesRoutes');
const stationsController = require('./routes/stationsController');
const resourcesController = require('./routes/resourcesController');
const driversController = require('./routes/driversController');
const transitController = require('./routes/transitController');
const companyController = require('./routes/companyController');
const tripsController = require('./routes/tripsController');
const filterController = require('./routes/filterController');
const LocationInfoController = require('./routes/locationInfoController');
const app = express.Router();


app.use('/resources', resourcesController);
app.use('/vehicles', vehiclesController);
app.use('/drivers', driversController);
app.use('/stations', stationsController);
app.use('/transit', transitController);
app.use('/company', companyController);
app.use('/trips', tripsController);
app.use('/filter', filterController);
app.use('/location-info', LocationInfoController);

app.use('/coupon', couponRoutes);

module.exports = app;